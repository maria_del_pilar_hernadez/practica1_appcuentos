package com.example.practica1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_cuento_tres.*
import kotlinx.android.synthetic.main.activity_ultimo_cuento.*

class ultimo_cuento : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ultimo_cuento)

        button9.setOnClickListener {

            val intent = Intent(this, ultima::class.java)
            startActivity(intent)
        }
    }
}