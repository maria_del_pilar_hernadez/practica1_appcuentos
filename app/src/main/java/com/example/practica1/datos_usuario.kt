package com.example.practica1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_cuento_tres.*
import kotlinx.android.synthetic.main.activity_datos_usuario.*

class datos_usuario : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_datos_usuario)
        val bundle : Bundle? = intent.extras

        bundle?.let {bundleLibriDeNull ->
            val nombres = bundleLibriDeNull.getString("key_nombres","Desconocido")
            val edad = bundleLibriDeNull.getString("key_edad","0")
            val apodos = bundleLibriDeNull.getString("key_apodo","")

            textView10.text = "$nombres"
            textView12.text = "$edad"
            textView14.text = "$apodos"
        }

        button5.setOnClickListener {

            val intent = Intent(this, cuento_uno::class.java)
            startActivity(intent)
        }
    }
}