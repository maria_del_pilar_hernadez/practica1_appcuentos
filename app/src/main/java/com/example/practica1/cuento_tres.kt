package com.example.practica1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_cuento_tres.*

class cuento_tres : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuento_tres)

            button4.setOnClickListener {

                val intent = Intent(this, ultimo_cuento::class.java)
                startActivity(intent)
            }
        }
    }
