package com.example.practica1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener{

            // 2.- Obtener la informacion de las cajas de texto

            val nombres = txtnombre.text.toString()
            val edad = edad.text.toString()
            val apodos=apodo.text.toString()

            //3.- validando que los nombres no estan vacios

            if(nombres.isEmpty()){
                Toast.makeText(this,"Debe ingresar sus nombre",Toast.LENGTH_LONG).show()
                return@setOnClickListener    // deberia deternese el codigo y vuelva a solicitar
            }
            // 4.- Validando que la edad no este vacia
            if(edad.isEmpty()){
                Toast.makeText(this,"Debe ingresar su edad",Toast.LENGTH_LONG).show()
                return@setOnClickListener    // deberia deternese el codigo y vuelva a solicitar
            }

            if(apodos.isEmpty()){
                Toast.makeText(this,"Debe ingresar su apodo",Toast.LENGTH_LONG).show()
                return@setOnClickListener    // deberia deternese el codigo y vuelva a solicitar
            }

            // 5.- validadndo que haya aceptado los terminos y condiciones


            var genero = if(masculino.isChecked)"masculino" else "femenino"

            val bundle = Bundle()
            bundle.apply {
                putString("key_nombres",nombres)
                putString("key_edad",edad)
                putString("key_apodo",apodos)
            }
            val intent = Intent(this,datos_usuario::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }


}